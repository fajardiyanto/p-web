@extends('layout.main')

@section('title', 'Login')

@section('container')
<section class="story-area left-text center-sm-text pos-relative">
    <div class="abs-tbl bg-2 w-20 z--1 dplay-md-none\"></div>
    <div class="abs-tbr bg-3 w-20 z--1 dplay-md-none"></div>
    <div class="container">
      <div class="heading">
        <img class="heading-img" src="" alt="">
        <div class="content">
                <h1 class="mb-4">Queen Burger</h1>
                <h4 class="mb-4-1">Login Account</h4>
                <div class="container col-md-6">
                    @if(\Session::has('alert'))
                        <div class="alert alert-danger">
                            <div>{{Session::get('alert')}}</div>
                        </div>
                    @endif
                    @if(\Session::has('alert-success'))
                        <div class="alert alert-success">
                            <div>{{Session::get('alert-success')}}</div>
                        </div>
                    @endif
    
                    @if(Session::has('message_login'))
                        <div class="alert alert-danger"><span class="glyphicon glyphicon-ok"></span><em> {!! session('message_login') !!}</em></div>
                    @endif
                    <form action="{{ route('login-post') }}" method="post">
                        {{ csrf_field() }}
                        <div class="heading">
                            <label for="email">Email:</label>
                            <input type="email" class="form-control" id="email" name="email">
                        </div>
                        <div class="heading">
                            <label for="alamat">Password:</label>
                            <input type="password" class="form-control" id="password" name="password">
                        </div>
                        <div class="heading">
                            <button type="submit" class="btn btn-md btn-primary">Login</button>
                            <a href="{{ route('register') }}" class="btn btn-md btn-warning">Register</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
     
      </div>
    </div>
  </section>
   
@endsection