@extends('layout.main')

@section('title', 'Login')

@section('container')
<div class="abs-tbl bg-2 w-20 z--1 dplay-md-none\"></div>
<div class="abs-tbr bg-3 w-20 z--1 dplay-md-none"></div>
<div class="container">
    <section class="main-section">
        <div class="content">
            <h1 class="mb-4">Queen Burger</h1>
            <h4 class="mb-4-1">Register Account</h4>
            <div class="container col-md-6">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form action="{{ route('register-store') }}" method="post">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="email">Email:</label>
                    <input type="email" class="form-control" id="email" name="email">
                </div>
                <div class="form-group">
                    <label for="alamat">Password:</label>
                    <input type="password" class="form-control" id="password" name="password">
                </div>
                <div class="form-group">
                    <label for="alamat">Password Confirmation:</label>
                    <input type="password" class="form-control" id="confirmation" name="confirmation">
                </div>
                <div class="form-group">
                    <label for="alamat">Name:</label>
                    <input type="text"  class="form-control" id="name" name="name">
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-md btn-primary">Submit</button>
                    <button type="reset" class="btn btn-md btn-danger">Cancel</button>
                </div>
            </form>
            </div>
        </div>
    </section>
</div>
@endsection